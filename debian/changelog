d3-format (1:1.0.2-4) UNRELEASED; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 11
  * Declare compliance with policy 4.3.0
  * Change section to javascript
  * Change priority to optional
  * Fix VCS fields
  * Add upstream/metadata
  * Move nodejs from node-d3-format recommended dependencies to dependencies
    (Closes: #900012)
  * Switch tests to pkg-js-tools
  * Fix debian/copyright BSD license, fixes
    copyright-refers-to-deprecated-bsd-license-file lintian warning
  * Mark libjs-d3-format as "Multi-Arch: foreign"

  [ Balint Reczey ]
  * Ack NMU

 -- Balint Reczey <rbalint@ubuntu.com>  Sat, 08 Jun 2019 23:16:20 +0200

d3-format (1:1.0.2-3.1) unstable; urgency=medium

  * Non-maintainer upload (Closes: #929617)
    - Bumping the version to over 1:1.0.2-3 because 1.0.2-3 was used by
      src:node-d3-format

 -- Balint Reczey <rbalint@ubuntu.com>  Sat, 08 Jun 2019 22:52:37 +0200

d3-format (1:1.0.2-1) unstable; urgency=medium

  * Take back my package, thank you very much.
  * Remove from team maintenance.

 -- Ximin Luo <infinity0@debian.org>  Mon, 09 Oct 2017 22:33:04 +0200

d3-format (1.0.2-2) unstable; urgency=medium

  * Use debhelper compat level 10.
  * Correct binary packages from arch:any to arch:all.
  * Use node-tap to run tests, instead of node-tape.

 -- Ximin Luo <infinity0@debian.org>  Mon, 21 Nov 2016 02:21:00 +0100

d3-format (1.0.2-1) unstable; urgency=medium

  * Initial release. (Closes: #844784)

 -- Ximin Luo <infinity0@debian.org>  Sat, 19 Nov 2016 04:00:20 +0100
